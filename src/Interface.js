const readline = require('readline');

class Interface {
    constructor(){
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    }

    readline(){
        return this.rl
    }
}

module.exports = Interface