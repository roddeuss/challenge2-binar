const Nilai = require("./Nilai");
const Interface = require("./Interface")



class PenilaianMataKuliah extends Interface{

    constructor(){
        super()
        this.mhsNilai = new Nilai()
    }

    pertanyaan(){
        console.log('Input Nilai Mahasiswa Mata Kuliah Basis Data')
        console.log('Ketik q Jika sudah selesai menginput nilai Basis Data')

        const tambahNilai = () => this.readline().question('Nilai : ', (answer) => {
            answer = answer
            if(answer == 'q'){
                this.#berhenti()
                return;
            }

            answer = Number(answer)

            if((answer !== '') && !isNaN(answer) && (answer != null) && (typeof(answer) == 'number')) {
                this.mhsNilai.addNilai(answer)
                tambahNilai()
            } else {
                console.log('Silahkan input angka atau q')
                tambahNilai()
            }
        })

        tambahNilai()
    }

    #berhenti(){
        console.log('')
        if(this.mhsNilai.countNilai() > 0) {
            console.log(`Nilai Tertinggi: ${this.mhsNilai.nilaiMax()}`)
            console.log(`Nilai Terendah: ${this.mhsNilai.nilaiMin()}`)
            console.log(`Urutan Nilai : [${this.mhsNilai.sortedNilai()}]`)
            console.log(`Rata Rata Nilai: ${this.mhsNilai.sumNilai()} / ${this.mhsNilai.countNilai()} = ${this.mhsNilai.avgNilai()}`)
            console.log(`Jumlah siswa Lulus : ${this.mhsNilai.siswaLulus().length}`)
            console.log(`Jumlah siswa Tida Lulus : ${this.mhsNilai.siswaTidakLulus().length} `)
        }
        else {
            console.log("Inputan Kosong")
        }

        this.#ulang()
    }

    #ulang(){
        this.readline().question("Apakah anda ingin kembali ? (y/n) \n", (answer) => {
            if (answer === "y") {
                this.pertanyaan();
            } else {
                this.readline().close()
            }
        })
    }
}

module.exports = PenilaianMataKuliah