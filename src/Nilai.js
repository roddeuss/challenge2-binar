class Nilai {
    nilaiMahasiswa = []
    kkm = 75

    constructor(nilaiMahasiswa, kkm) {
        this.nilaiMahasiswa = (nilaiMahasiswa != undefined && Array.isArray(nilaiMahasiswa)) ? nilaiMahasiswa: []
        this.kkm = kkm || 75
    }

    addNilai(nilai){
        this.nilaiMahasiswa.push(nilai)
    }

    nilai(){
        return this.nilaiMahasiswa
    }

    sortedNilai(){
        const bubleSort = (arr) => {
            var len = arr.length
            for(var i = len-1; i>=0; i--){
                for(var j = 1; j<=i; j++){
                    if(arr[j-1]>arr[j]){
                        var temp = arr[j-1]
                        arr[j-1] = arr[j]
                        arr[j] = temp
                    }
                }
            }
            return arr;
        }
        return bubleSort(this.nilaiMahasiswa)
    }

    nilaiMax(){
        // return this.SortedNilai()[this.nilaiMahasiswa.length - 1]
        return this.sortedNilai()[this.nilaiMahasiswa.length - 1]
    }

    nilaiMin(){
        return this.sortedNilai()[0]
    }

    siswaLulus(){
        return this.nilaiMahasiswa.filter((val) => {
            return val >= this.kkm
        })
    }

    siswaTidakLulus(){
        return this.nilaiMahasiswa.filter((val) => {
            return val < this.kkm
        })
    }

    sumNilai(){
        let data = this.nilaiMahasiswa
        let sum = 0

        data.forEach((nilai) => {
            sum += nilai
        })
        return sum
    }

    countNilai(){
        return this.nilaiMahasiswa.length
    }

    avgNilai(){
        return Math.round(this.sumNilai() / this.countNilai())
    }
    
}

module.exports = Nilai
